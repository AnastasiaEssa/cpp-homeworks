﻿#include <iostream>
#include <string>

int main()
{
    std::cout << "Enter anything: ";
    std::string phrase;
    std::getline(std::cin, phrase);

    std::cout << "You entered: " << phrase << "\n";
    std::cout << "The lenght = " << phrase.length() << "\n";
    std::cout << "First symbol is " << phrase[0] << "\n";
    std::cout << "Last symbol is " << phrase[phrase.size()-1] << "\n";
}
