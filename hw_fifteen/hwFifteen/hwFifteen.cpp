﻿#include <iostream>

void printOddNumbers(bool isOdd, const uint16_t limit) {
	for (int counter = isOdd; counter <= limit && counter % 2 == isOdd; counter += 2) {
		std::cout << counter << "\n";
	}
}

int main() {
	printOddNumbers(true, 11);
}