﻿#include <iostream>
#include <cmath>

class Vector {
    public:
        Vector() : x(0), y(0), z(0)
        {}
        Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
        {}
        void Show() {
            std::cout << "Vector lenght = " << sqrt(x * x + y * y + z * z) << '\n';
        };
    private:
        double x, y, z; 
};

int main()
{
    Vector v(2, 3, 4);
    v.Show();
}