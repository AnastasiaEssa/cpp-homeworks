﻿#include <iostream>
#include <time.h>

struct tm buf;

int main()
{
    const int size = 6;
    int array[size][size];

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";
        }
                std::cout << '\n';
    }

    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int day = buf.tm_mday % 2;
    int sum = 0;
    for (int k = 0; k < size; k++) {
        sum += array[day][k];
    }
    std::cout << "Line sum = " << sum;
}
