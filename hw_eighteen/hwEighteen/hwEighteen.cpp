﻿#include <iostream>

class Stack {
private:
	int NumArray = 0;
	int* StackArray = new int[NumArray];
	void DeleteLastElement() {
		std::cout << "Deleted last element" << std::endl;
		int* NewArray = new int[NumArray - 1];
		for (int i = 0; i < (NumArray - 1); i++) {
			NewArray[i] = StackArray[i];
		}
		delete[] StackArray;
		NumArray--;
		StackArray = NewArray;
	}

public:
	Stack() {
	}

	void Push(int Value) {
		int* NewArray = new int[NumArray + 1];
		std::cout << "Added new element: " << Value << std::endl;
		for (int i = 0; i < NumArray; i++) {
			NewArray[i] = StackArray[i];
		}
		NewArray[NumArray] = Value;
		delete[] StackArray;
		NumArray++;
		StackArray = NewArray;
	}

	int Pop() {
		int a = StackArray[NumArray - 1];
		DeleteLastElement();
		return a;
	}

	void Print() {
		for (int i = 0; i < NumArray; i++) {
			std::cout << *(StackArray + i) << " ";
		}
		std::cout << std::endl;
	}

	~Stack() {
		delete[] StackArray;
	}
};

int main() {
	Stack test;
	test.Push(1);
	test.Push(4);
	test.Push(7);
	test.Push(7);
	test.Pop();
	test.Print();
}